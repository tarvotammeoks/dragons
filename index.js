var fs = require('fs')
var express = require('express')
var app = express()

app.use(express.static(__dirname + '/webapp/'))

app.get('*', (req, res) => {
  var htmlFile = __dirname + '/webapp/index.html'
  fs.readFile(htmlFile, (err, html) => {
    res.set('Content-Type', 'text/html')
    res.statusCode = 200
    res.send(html)
  })
})

app.listen(3344, () => console.log('Server running on http://localhost:3344/'))
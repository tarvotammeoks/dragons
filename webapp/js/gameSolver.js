Dragons.controller('gameSolver', function($scope, $http) {
  $scope.numberOfGames = 1
  $scope.wins = 0
  $scope.losses = 0

  $scope.run = function () {
    for (var i = 0; i < $scope.numberOfGames; i++) {
      var success = function (response) {
        game = response.data
        var gameId = game.gameId

        var stats = game.knight

        function findHighestStats(obj){
          var highest = 0
          var arr = []

          for (var prop in obj) {
            if (obj[prop] > highest ) { 
              arr = []
              highest = obj[prop]
              arr[prop] = highest
            } else if (obj[prop] == highest) {
              arr[prop] = highest
            }
          }
          return arr
        }

        var highestStats = findHighestStats(stats)
        var maxStat, maxValue, maxDragonStat

        for (var prop in highestStats) {
          maxStat = prop
          maxValue = stats[prop] +2
        }

        switch (maxStat) {
          case 'attack':
            maxDragonStat = 'scaleThickness'
            break
          case 'armor':
            maxDragonStat = 'clawSharpness'
            break
          case 'agility':
            maxDragonStat = 'wingStrength'
            break
          case 'endurance':
            maxDragonStat = 'fireBreath'
            break
        }
        
        var dragon = {
          scaleThickness: game.knight.attack,
          clawSharpness: game.knight.armor,
          wingStrength: game.knight.agility,
          fireBreath: game.knight.endurance
        }

        dragon[maxDragonStat] = maxValue
        
        var counter = 1
        for (var prop in dragon) {
          if (prop != maxDragonStat && dragon[prop] > 0 && counter < 3) {
            dragon[prop] = dragon[prop] -1
            counter++
          }
        }

        $scope.sendDragon(gameId, dragon)
      }

      $http.get('http://www.dragonsofmugloar.com/api/game').then(success)
    }
  }

  $scope.sendDragon = function (gameId, dragon) {
    var success = function (response) {
      if (response.data.status == 'Victory') {
        $scope.wins += 1
      } else {
        $scope.losses += 1
      }
      $scope.result = response.data.status + '. ' + response.data.message + '. Overall wins: ' + $scope.wins + ' / losses: ' + $scope.losses
    }

    $http.put('http://www.dragonsofmugloar.com/api/game/' + gameId + '/solution', {dragon}).then(success)
  }

  $scope.reset = function () {
    $scope.result = ''
    $scope.losses = 0
    $scope.wins = 0
  }

})